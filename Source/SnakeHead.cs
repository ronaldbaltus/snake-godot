using Godot;
using System;

public class SnakeHead : SnakeTail
{
    [Signal]
    public delegate void OnAppleEaten(Apple apple);

    [Signal]
    public delegate void OnCollision();

    private Vector2 Direction = new Vector2(0f, 1f);
    private DateTime LastTurn = DateTime.Now;
    // Time between direction processing
    private TimeSpan TurnSpeed = new TimeSpan(0, 0, 0, 0, 400);

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();
    }

    public override void _Input(InputEvent e)
    {
        if (e.IsActionReleased("ui_up"))
        {
            Direction = new Vector2(0f, -1f);
        }
        else if (e.IsActionReleased("ui_down"))
        {
            Direction = new Vector2(0f, 1f);
        }
        else if (e.IsActionReleased("ui_left"))
        {
            Direction = new Vector2(-1f, 0f);

        }
        else if (e.IsActionReleased("ui_right"))
        {
            Direction = new Vector2(1f, 0f);
        }
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (DateTime.Now < LastTurn + TurnSpeed)
        {
            return;
        }

        // Out of bounds check
        if (Position.x < 0f || Position.x > 1920f || Position.y < 0f || Position.y > 1080f) 
        {
            EmitSignal(nameof(OnCollision));
            return;
        }

        LastTurn = DateTime.Now;

        // calc new pos
        var targetPos = this.Position + (Direction * StepSize);

        MoveToPosition(targetPos);


    }

    public override void OnAreaEntered(Area2D area2d)
    {
        base.OnAreaEntered(area2d);

        var hitNode = area2d.GetParent();

        if (hitNode is SnakeTail snakeTail) 
        {
            // Check if it is not the first one behind us.
            if (snakeTail.GetIndex() > GetIndex() + 1)
                EmitSignal(nameof(OnCollision));

            return;
        }

        if (hitNode is Apple apple) 
        {
            // we hit an apple
            EmitSignal(nameof(OnAppleEaten), apple);
        }
    }
}
