using Godot;
using System;

public class SnakeTail : Node2D
{
    [Signal]
    delegate void MoveStarted(Vector2 StartPosition, Vector2 TargetPosition);

    [Signal]
    delegate void MoveCompleted(Vector2 EndPosition);


    // Duration of movement per turn.
    [Export]
    public float MoveDuration = 0.3f;

    [Export]
    public float StepSize = 128f;

    protected Tween MoveTween = null;
    protected Vector2 MoveFrom = new Vector2();
    protected Vector2 MoveTo = new Vector2();
    protected Area2D CollisionArea = null;

        // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        MoveTween = GetNode<Tween>("MoveTween");
        CollisionArea = GetNode<Area2D>("Area2D");

        MoveTween.Connect("tween_completed", this, nameof(OnMoveTweenCompleted));
        CollisionArea.Connect("area_entered", this, nameof(OnAreaEntered));
        CollisionArea.Connect("area_exited", this, nameof(OnAreaExited));
    }

    public void MoveToPosition(Vector2 targetPos)
    {
        if (Position == targetPos)
        {
            GD.Print("Already at position");
            return;
        }

        this.MoveFrom = Position;
        this.MoveTo = targetPos;

        MoveTween.InterpolateProperty(
            this,
            "position", 
            Position,
            targetPos,
            MoveDuration,
            Tween.TransitionType.Cubic,
            Tween.EaseType.InOut
        );
        
        EmitSignal(nameof(MoveStarted), Position, targetPos);

        MoveTween.Start();
    }

    public void OnMoveTweenCompleted(object obj, NodePath path)
    {
        CollisionArea.Monitoring = true; // enable monitoring after first move.
        EmitSignal(nameof(MoveCompleted), MoveFrom, Position);
    }

    public void Follow(SnakeTail tail)
    {
        tail.Connect(nameof(MoveStarted), this, nameof(OnFolloweeMoveStarted));
        tail.Connect(nameof(MoveCompleted), this, nameof(OnFolloweeMoveCompleted));
    }

    public void OnFolloweeMoveCompleted(Vector2 startPos, Vector2 endPos)
    {
        // Go to the startpos of my followee
        //MoveToPosition(endPos);
    }
    public void OnFolloweeMoveStarted(Vector2 startPos, Vector2 endPos)
    {
        // Go to the startpos of my followee
        MoveToPosition(startPos);
    }

    public virtual void OnAreaEntered(Area2D area2d)
    {

    }
    public virtual void OnAreaExited(Area2D area2d)
    {

    }
}