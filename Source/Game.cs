using Godot;
using System;

public class Game : Node2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private Vector2 AreaSize = new Vector2(1920, 1080);
	private float CellSize = 128f;

	private Node2D Snake;

	private Label ScoreLabel = null;
	private RichTextLabel GameOverLabel = null;
	private int Score = 0;
	private bool Gameover = false;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Snake = GetNode<Node2D>("Snake");

		AddTail();
		SpawnApple();

		var head = Snake.GetNode<SnakeHead>("SnakeHead");

		head.Connect(nameof(SnakeHead.OnAppleEaten), this, nameof(SnakeAteApple));
		head.Connect(nameof(SnakeHead.OnCollision), this, nameof(GameOver));

		ScoreLabel = GetNode<Label>("CanvasLayer/Score");
		GameOverLabel = GetNode<RichTextLabel>("CanvasLayer/GameOver");
	}

    public override void _Input(InputEvent e)
    {
        base._Input(e);

		if (!Gameover) return;

		if (e.IsActionReleased("ui_accept"))
		{
			// user wants to restart
			GetTree().ChangeScene("res://Game.tscn");
		}
    }

	public void AddTail()
	{
		var lastTail = Snake.GetChild<SnakeTail>(Snake.GetChildCount() - 1);
		var resource = GD.Load<PackedScene>("res://Scenes/SnakeTail.tscn");
		var newTail = (SnakeTail)resource.Instance();

		Snake.AddChild(newTail);
		newTail.Follow(lastTail);
		newTail.Position = lastTail.Position;
	}

	public void SpawnApple()
	{
		var resource = GD.Load<PackedScene>("res://Scenes/Apple.tscn");
		var newApple = (Apple)resource.Instance();

		var rnd = new Random();

		newApple.Position = new Vector2(
		 	(CellSize * 0.5f) + (CellSize * rnd.Next(0, (int)(AreaSize.x / CellSize))),
			(CellSize * 0.5f) + (CellSize * rnd.Next(0, (int)(AreaSize.y / CellSize)))
		);

		AddChild(newApple);
	}

	public void SnakeAteApple(Apple apple)
	{
		RemoveChild(apple);
		SpawnApple();
		AddTail();
		Score++;
		ScoreLabel.Text = $"Apples eaten: {Score}";
	}

	public void GameOver()
	{
		RemoveChild(Snake);
		GameOverLabel.Visible = true;
		Gameover = true;
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
